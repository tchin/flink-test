import org.apache.flink.api.scala.createTypeInformation
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.test.util.MiniClusterWithClientResource
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.{AfterAll, BeforeAll, Test, TestInstance}
import org.wikimedia.flinktest.IncrementMapFunction

import java.util

@TestInstance(TestInstance.Lifecycle.PER_CLASS) class StreamingJobIntegrationJUnitTest {

  val flinkCluster = new MiniClusterWithClientResource(new MiniClusterResourceConfiguration.Builder()
    .setNumberSlotsPerTaskManager(2)
    .setNumberTaskManagers(1)
    .build)

  @BeforeAll def before(): Unit = {
    flinkCluster.before()
  }

  @AfterAll def after(): Unit = {
    flinkCluster.after()
  }

  @Test def testIncrement(): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    // configure your test environment
    env.setParallelism(2)

    // values are collected in a static variable
    CollectSink.values.clear()

    // create a stream of custom elements and apply transformations
    env.fromElements(1L, 21L, 22L)
      .map(new IncrementMapFunction())
      .addSink(new CollectSink())

    // execute
    env.execute()

    // verify your results
    assertTrue(CollectSink.values.containsAll(util.Arrays.asList(2L, 22L, 23L)))
  }
}