import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.wikimedia.flinktest.IncrementMapFunction

class IncrementMapFunctionScalaTest extends AnyFlatSpec with Matchers {

  "IncrementMapFunction" should "increment values" in {
    // instantiate your function
    val incrementer: IncrementMapFunction = new IncrementMapFunction()

    // call the methods that you have implemented
    incrementer.map(2) should be (3)
  }
}