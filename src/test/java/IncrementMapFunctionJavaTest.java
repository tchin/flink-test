import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.wikimedia.flinktest.IncrementMapFunction;

public class IncrementMapFunctionJavaTest {

    @Test
    public void testIncrement() throws Exception {
        // instantiate your function
        IncrementMapFunction incrementer = new IncrementMapFunction();

        // call the methods that you have implemented
        Assertions.assertEquals(3L, incrementer.map(2L));
    }
}